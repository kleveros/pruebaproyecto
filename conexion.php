<?php
    
    function conectar()
   {
        $servername = "localhost";
        $username = "root";  
        $database = "regnotas";
        $password = "";
    
        $conn = mysqli_connect($servername, $username, $password, $database);
    
        if(!$conn)
        {
            die("Connection failed: " . mysqli_connect_error());
        }
        echo "Conectado";

        return $conn;
   }

   function disconnectDB($conn)
   {

     $close = mysqli_close($conn) 
        or die("Ha sucedido un error inexperado en la desconexion de la base de datos");

    return $close;
    }


?>
