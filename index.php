<!DOCTYPE html>
<html>
	<head>
	<meta charset="ISO-8859-1">
		<title>UESFA</title>
			</head>
	
	<body>
		<h1>Sistema de Registro de Notas U.E.S.F.A.</h1>
        <form action="datos.php" method="post" onclick="Enviar">
            <div class="row">
                <div class="col-25">
                    <label for="lblcurso"> Curso: </label>
                </div>
                
                <div class="col-75">
                    <select name="slctcurso" id="curso">
				
				<?php
				    include ("conexion.php");
				    $conn = conectar();
                    $rec = $conn->query("SELECT * FROM curso");
                    
                    while ($row = mysqli_fetch_array($rec)) 
                    {
                        echo "<option value='".$row['idcurso']."' ";
                        if ($_POST['curso'] = $row['idcurso']) 
                        
                            //echo "selected";
                            echo ">";
                            echo $row['cursoname'];
                            echo "</option>";
                    }
                 ?>

			        </select>
			     </div>
			</div>
			
			<div class="row">
			    <div class="col-25">
			        <label for="lblasignatura">Asignatura: </label>
			    </div>
			    <div class="col-75">
			        <select name="slctasignatura" id="asignatura">
				    <?php
				    //inc ("conexion.php")
				    //$conn = conectar();
                    $rec = $conn->query("SELECT * FROM asignatura");
                    
                    while ($row = mysqli_fetch_array($rec)) 
                    {
                        echo "<option value='".$row['idasignatura']."' ";
                        if ($_POST['curso'] = $row['idasignatura']) 
                        
                            //echo "selected";
                            echo ">";
                            echo $row['asignatura'];
                            echo "</option>";
                    }
                    ?>
			        </select>
			    </div>
			</div>
			
			<div class="row">
			    <div class="col-25">
			        <label for="lblname"> Nombre: </label>
			    </div>
			    <div class="col-75">
			        <input type="text" id="nombre" name="txtname" placeholder="Nombre del estudiante...">
                </div>
            </div>
			
			<div class="row">
                    <div class="col-25">
                        <label for="lbln1">Nota 1</label>
                    </div>
                    <div class="col-75">
                        <input type="number" id="n1" name="txtn1" placeholder="Calificacion 1.." min="1" max="10">
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-25">
                        <label for="lbln2">Nota 2</label>
                    </div>
                    <div class="col-75">
                        <input type="number" id="n2" name="txtn2" placeholder="Calificacion 2..." min="1" max="10">
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-25">
                        <label for="lbln3">Nota 3</label>
                    </div>
                    <div class="col-75">
                        <input type="number" id="n3" name="txtn3" placeholder="Calificacion 3..." min="1" max="10">
                    </div>
                </div>
                  
                <div class="row">
                    <div class="col-25">
                        <label for="lblemail">Email</label>
                    </div>
                    <div class="col-75">
                        <input type="email" id="email" name="txtemail" placeholder="Ingrese el correo...">
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-25">
                        <label for="subject">Observaciones</label>
                    </div>
                    <div class="col-75">
                        <textarea id="observaciones" name="subject" placeholder="Escriba una observacion..." style="height:200px"></textarea>
                    </div>
                </div>
                
                <div class="row">
                    <input type="submit" value="Enviar">
                </div>
            </form>
        </div>
	</body>
</html>